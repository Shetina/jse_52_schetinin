package ru.t1.schetinin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.repository.model.ISessionRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.model.ISessionService;
import ru.t1.schetinin.tm.model.Session;
import ru.t1.schetinin.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}