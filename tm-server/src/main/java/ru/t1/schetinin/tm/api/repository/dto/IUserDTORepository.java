package ru.t1.schetinin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.enumerated.Role;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @Nullable Role role) throws Exception;

    @Nullable
    UserDTO findByLogin(@NotNull String login) throws Exception;

    @Nullable
    UserDTO findByEmail(@NotNull String email) throws Exception;

    Boolean isLoginExists(@NotNull String login) throws Exception;

    Boolean isEmailExists(@NotNull String email) throws Exception;

}
